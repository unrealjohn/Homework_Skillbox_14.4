
#include <iostream>

int main()
{
    std::string TestString = "Hi world";
    //String text
    std::cout << TestString << "\n";
    //String length
    std::cout << TestString.length() << "\n";
    //First and last symbols of String
    std::cout << TestString[0] << "\n";
    std::cout << TestString[TestString.length()-1] << "\n";
}

